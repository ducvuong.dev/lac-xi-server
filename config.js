exports.ARR_ITEM = [
  {
    type          : 0,
    name          : "Thẻ cào 10K",
    maximum       : 1000,
    percent       : 50,
    amount        : 0,
    save_in_gift  : true, //lưu lại trong hộp quà (true) || lưu lại trong túi (false)
    upgrade       : -1,   //giữ key sẽ  update thành
    condi_merge   : -1,   // điều kiện để có thể merge (vd: 4 mảnh thì được 1 thẻ)
    is_gift       : false //có thể tặng
  },

  {
    type          : 1,
    name          : "Thẻ cào 50K",
    maximum       : 500,
    percent       : 30,
    amount        : 0,
    save_in_gift  : true,
    upgrade       : -1,
    condi_merge   : -1,
    is_gift       : false
  },

  {
    type          : 2,
    name          : "Mảnh Ghép Long",
    maximum       : 50000,
    percent       : 100,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : 6,
    condi_merge   : 4,
    is_gift       : true
  },

  {
    type          : 3,
    name          : "Mảnh Ghép Lân",
    maximum       : -1,
    percent       : 400,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : 7,
    condi_merge   : 4,
    is_gift       : true
  },

  {
    type          : 4,
    name          : "Mảnh Ghép Quy",
    maximum       : -1,
    percent       : 400,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : 8,
    condi_merge   : 4,
    is_gift       : true
  },

  {
    type          : 5,
    name          : "Mảnh Ghép Phụng",
    maximum       : 100,
    percent       : 18,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : 9,
    condi_merge   : 4,
    is_gift       : true
  },

  {
    type          : 6,
    name          : "Thẻ Long",
    maximum       : 0,
    percent       : 0,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : -1,
    condi_merge   : -1,
    is_gift       : true
  },

  {
    type          : 7,
    name          : "Thẻ Lân",
    maximum       : 1,
    percent       : 1,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : -1,
    condi_merge   : -1,
    is_gift       : true
  },

  {
    type          : 8,
    name          : "Thẻ Quy",
    maximum       : 1,
    percent       : 1,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : -1,
    condi_merge   : -1,
    is_gift       : true
  },

  {
    type          : 9,
    name          : "Thẻ Phụng",
    maximum       : 0,
    percent       : 0,
    amount        : 0,
    save_in_gift  : false,
    upgrade       : -1,
    condi_merge   : -1,
    is_gift       : true
  }
];

exports.QUESTION = [
  {
    type        : 0,
    description : 'Bạn có thích thể loại game này không?',
    input       : false,
    hide        : false,
    option      : ['A', 'B', 'C', 'D', 'E', 'F']
  },
  {
    type        : 1,
    description : 'Bạn có mê thể loại game này không?',
    input       : true,
    hide        : false,
    option      : []
  },
  {
    type        : 2,
    description : 'Bạn có ghiền thể loại game này không?',
    input       : true,
    hide        : false,
    option      : []
  },
  {
    type        : 3,
    description : 'Bạn có đam mê thể loại game này không?',
    input       : false,
    hide        : false,
    option      : ['J', 'K', 'M', 'L', 'E', 'V']
  },
  {
    type        : 4,
    description : 'Bạn có đam mê thể loại game này không?',
    input       : true,
    hide        : false,
    option      : []
  },
  {
    type        : 3,
    description : 'Bạn có đam mê thể loại game này không?',
    input       : false,
    hide        : false,
    option      : ['J', 'K', 'M', 'L', 'E', 'V']
  },
  {
    type        : 3,
    description : 'Bạn có đam mê thể loại game này không?',
    input       : true,
    hide        : false,
    option      : []
  }
]

exports.MISSIONS = [
  {
    type        : 0,
    description : 'Đăng nhập vào game\nmỗi ngày',
    bonus       : 1,       //thêm lượt cho user khi hoàn thành nhiệm vụ
    condition   : null     //điều kiện kèm theo để hoàn thành nhiệm vụ
  },
  {
    type        : 1,
    description : 'Đăng nhập vào khoảng\n12h - 13h, 18h - 19h',
    bonus       : 1,
    condition   : [12,13,18,19]
  },
  {
    type        : 2,
    description : 'Mời 1 bạn mới thành công',
    bonus       : 1,
    condition   : null
  },
  {
    type        : 3,
    description : 'Mời 5 bạn mới thành công',
    bonus       : 10,
    condition   : null
  },
  {
    type        : 4,
    description : 'Quay 1 lần vòng quay THP',
    bonus       : 1,
    condition   : null
  },
  {
    type        : 5,
    description : 'Quay 5 lần vòng quay THP',
    bonus       : 10,
    condition   : null
  },
  {
    type        : 6,
    description : 'Share Game lên FB',
    bonus       : 1,
    condition   : null
  },
  {
    type        : 7,
    description : 'Hoàn thành khảo sát',
    bonus       : 1,
    condition   : null,
    question    : this.QUESTION
  },
]

exports.GLOBAL_REDIS = {
  port    : 6379,
  host    : '127.0.0.1'
}, //global

exports.LENGTH_REDIS = 2;

exports.REDIS_CONF = [
  {
    port    : 6380,
    host    : '127.0.0.1'
  }, //save user
  {
    port    : 6381,
    host    : '127.0.0.1'
  }, //save user
]