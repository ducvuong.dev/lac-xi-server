const admin            = require('firebase-admin');
const serviceAccount   = require('../service_account.json');
const CONFIG           = require('../config');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

const db               = admin.firestore();

//user test, fake API
const users = [
  {
    megaID        : '10009',
    name          : 'Nguyễn Văn A',
    create_time   : 1512215739801,
    friends       :[
      '10000','10001','10002','10003'
    ]
  },
  {
    megaID        : '10000',
    name          : 'Nguyễn Văn B',
    create_time   : 1518214739801,
    friends       : [ '10009' ]
  },
  {
    megaID        : '10001',
    name          : 'Nguyễn Văn C',
    create_time   : 15521473801,
    friends       : [ '10009' ]
  },
  {
    megaID        : '10002',
    name          : 'Nguyễn Văn D',
    create_time   : 15121473801,
    friends       : [ '10009' ]
  },
  {
    megaID        : '10003',
    name          : 'Nguyễn Văn E',
    create_time   : 15523477801,
    friends       : [ '10009' ]
  },
  {
    megaID        : '10004',
    name          : 'Nguyễn Văn F',
    create_time   : 15121477801,
    friends       : [ '10007', '10008' ]
  }
]

exports.initData = () => {
  return new Promise(async (resv, rej) => {

    for (let element of users) {
      await db.collection('users')
              .doc(element.megaID)
              .set(element)
    }

    return resv('ok');

  });
}

exports.getUsers = () => {
  return new Promise((resv, rej) => {
    db.collection('users')
      .get()
      .then(result => {
        let lsUser = result.docs.map(doc => doc.data());
        resv(lsUser);
      })
      .catch(err => rej(err));
  });
}

exports.getUserBy = (megaID) => {
  return new Promise((resv, rej) => {
    db.collection('users')
      .where('megaID', '==', megaID)
      .get()
      .then(snapshot => {
        if (snapshot.docs.length === 0) {
          return resv(null);
        }
        return resv(snapshot.docs[0].data());
      })
      .catch(err => {
        return rej(null);
      });
  });
}