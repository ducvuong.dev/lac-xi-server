/** 
 * @param {String} key
 * megaID
*/

const redis           = require('redis');
const config          = require('../config');
const { hashCode }    = require('../utils/hash_code');
const arrUtil         = require('../utils/array_util');

const clientGlobal    = redis.createClient({
  port  : config.GLOBAL_REDIS.port,
  host  : config.GLOBAL_REDIS.host
});

var clients     = [];
for (let ele of config.REDIS_CONF) {
  let tmpClient = redis.createClient({
    port: ele.port,
    host: ele.host
  });
  clients.push(tmpClient);
}

clientGlobal.on('connect', () => {
  console.log('you are now connected now');
});



//-------------------item----------------------------------------------
exports.initItem = (items) => {
  let lsItem = JSON.stringify(items);
  clientGlobal.set('items', lsItem);
}

exports.updateLsItem = (lsItem) => {
  return new Promise((resv, rej) => {
    let ls = JSON.stringify(lsItem);
    clientGlobal.set('items', ls, (err, reply) => {
      if (err) return rej(err);
      return resv(reply);
    });
  });
}

exports.getAllItem = () => {
  return new Promise((resv, rej) => {
    clientGlobal.get('items', (err, reply) => {
      if (err) return rej(null);
      return resv(reply);
    });
  });
}
//-------------------item----------------------------------------------

//-------------------user----------------------------------------------
exports.initUser = (id, key, field, value) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(id) % config.LENGTH_REDIS);
    clients[index].hset(key, field, value, (err, reply) => {
      return resv(reply);
    });
  });
}

exports.getCardBillion = (key, field) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, field, (err, reply) => {
      if (err) return rej(null);
      return resv(reply);
    });
  });
}

exports.updateUser = (key, field, value) => {
  let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hset(key, field, value, (err, reply) => {
      if (err) console.log(err);
    });
}

exports.updateMultiFieldUser = (key, field1, value1, field2, value2, field3, value3) => {
  let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
  clients[index].hmset(key, field1, value1, field2, value2, field3, value3, (err, reply) => {
    if (err) console.log(err);
  });
}

exports.getTurnUser = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'turns', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.getInventory = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'inventories', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.getNotice = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'notices', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.getTurn = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'turns', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.getAllMissionUser = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'missions', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.getHistoryUser = (key) => {
  return new Promise((resv, rej) => {
    let index = Math.abs(hashCode(key) % config.LENGTH_REDIS);
    clients[index].hget(key, 'histories', (err, result) => {
      if (err) return rej(null);
      return resv(result);
    });
  });
}

exports.resetMisison = () => {
  for (let c of clients) {
    c.keys('*', async (err, reply) => {

      if (err)  {
        console.log(err);
        return rej(err);
      }

      if (reply.length > 0) {
        for (let c of reply) {
          let index = Math.abs(hashCode(c) % config.LENGTH_REDIS);
          clients[index].hget(c, 'missions', (err, reply) => {

            if (err) {
              console.log(err);
              return rej(err);
            }

            let missionUpdate = arrUtil.filterMission(reply);
            this.updateUser(c, 'missions', missionUpdate);

          });
        }
      }

    });
  }
}
//-------------------user----------------------------------------------

//-------------------quote---------------------------------------------
exports.initMission = (missions) => {
  const lsMission = JSON.stringify(missions);
  clientGlobal.set('missions', lsMission);
}

exports.getAllMission = () => {
  return new Promise((resv, rej) => {
    clientGlobal.get('missions', (err, result) => {
      return resv(result);
    });
  });
}
//-------------------quote---------------------------------------------