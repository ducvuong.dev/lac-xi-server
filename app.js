const express             = require('express');
const app                 = express();
const bodyParser          = require('body-parser');
const cors                = require('cors');

const shedule             = require('./utils/schedule');
const readFile            = require('./utils/read_data');

const wheelRouteRedis     = require('./route_redis/wheel_route_redis');
const inventoryRouteRedis = require('./route_redis/inventory_route_redis');
const missionRouteRedis   = require('./route_redis/mission_route_redis');
const itemRouteRedis      = require('./route_redis/item_route_redis');
const giftRoute           = require('./route_redis/gift_route');
const noticeRoute         = require('./route_redis/notice_route');

readFile.initData('./data.xlsx')
    .then(result => {
      
      app.use(bodyParser.urlencoded({
        extended: true
      }));
      app.use(bodyParser.json());
      
      app.use(cors());
      app.use('/api/wheel', wheelRouteRedis);
      app.use('/api/inventory', inventoryRouteRedis);
      app.use('/api/mission', missionRouteRedis);
      app.use('/api/merge', itemRouteRedis);
      app.use('/api/gift', giftRoute);
      app.use('/api/notice', noticeRoute);
      // app.use('/loaderio-91dbbb589f7eceb9f50538d818847502.txt', (req, res) => res.send('loaderio-91dbbb589f7eceb9f50538d818847502'));
      
      shedule.reset();
    
      const PORT = process.env.PORT || 3000;
    
      app.listen(PORT, async () => {
        console.log(`App listening on port ${PORT}`);
      });

    })
    .catch(err => {
      console.log(err);
    });