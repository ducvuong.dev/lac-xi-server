const cron      = require('node-cron');
const redis     = require('../redis/redis');

exports.reset = () => {
  cron.schedule('59 59 23 * * *', async () => {

    try {
      redis.resetMisison();
      await resetAmountItem();
    }
    catch(err) {
      console.log(err);
    }

  });
}

async function resetAmountItem () {
  let strItem = await redis.getAllItem();
  if (strItem !== null && strItem !== undefined) {
    let lsItem = JSON.parse(strItem);
    for (let e of lsItem) {
      e.amount = 0;
    }
    await redis.updateLsItem(lsItem);
  }
}