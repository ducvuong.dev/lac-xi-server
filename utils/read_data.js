const xlsxFile      = require('read-excel-file/node');
const config        = require('../config');
const redis         = require('../redis/redis');

exports.initData = (path) => {
  return new Promise((resv, rej) => {

    //read sheet item
    xlsxFile(path, { sheet: 'item' })
      .then(rowItem => {

        let lsItem = initLsItem(rowItem);
        if (lsItem.length <= 0) throw 'error';
        
        config.ARR_ITEM = lsItem;
        redis.initItem(lsItem);

        //read sheet mission
        xlsxFile(path, { sheet: 'mission' })
          .then(rowMission => {
            
            //read sheet question
            xlsxFile(path, { sheet: 'question' })
              .then(rowQuestion => {

                let resultQuesAndMiss = initQuestion(rowQuestion, rowMission);
                config.MISSIONS = resultQuesAndMiss.lsMission;
                config.QUESTION = resultQuesAndMiss.lsQuestion;
                redis.initMission(resultQuesAndMiss.lsMission);

                resv({
                  items     : lsItem,
                  missions  : resultQuesAndMiss.lsMission,
                  questions : resultQuesAndMiss.lsQuestion
                });

              })
              .catch(err => rej(err));

          })
          .catch(err => rej(err));

      })
      .catch(err => rej(err));

  });
}

function initLsItem(rows) {
  let lsItem  = [];
  let percent = 0;

  for (let i=1; i<rows.length; i++) {
    percent += rows[i][3];
    lsItem.push({
      type          : rows[i][0],
      name          : rows[i][1],
      maximum       : rows[i][2],
      percent       : rows[i][3],
      amount        : rows[i][4],
      save_in_gift  : rows[i][5],
      upgrade       : rows[i][6],
      condi_merge   : rows[i][7],
      is_gift       : rows[i][8]
    });
  }

  if (percent !== rows[0][9]) return [];
  return lsItem;
}

function initMission(rows) {
  let lsMission = [];
  for (let i=1; i<rows.length; i++) {
    let str = rows[i][1].replace('/n', '\n');
    lsMission.push({
      type        : rows[i][0],
      description : str,
      bonus       : rows[i][2],
      condition   : rows[i][3],
      question    : rows[i][4]
    });
  }

  return lsMission;
}

function initQuestion(rowsQuestion, rowMission) {

  let lsMission = initMission(rowMission);

  let lsQuestion = [];
  for (let i=1; i<rowsQuestion.length; i++) {
    let options = [];
    if (rowsQuestion[i][4] !== null) {
      options = rowsQuestion[i][4].split(',');
    }
    lsQuestion.push({
      type        : rowsQuestion[i][0],
      description : rowsQuestion[i][1],
      input       : rowsQuestion[i][2],
      hide        : rowsQuestion[i][3],
      option      : options
    });
  }

  let tempMission      = lsMission.find(e => { return e.question === true });
  tempMission.question = lsQuestion;

  return {
    lsMission     : lsMission,
    lsQuestion    : lsQuestion
  }
}