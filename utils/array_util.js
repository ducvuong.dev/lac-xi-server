exports.inclcudes = (obj, list) => {
  for (let element of list) {
    if (element.type === obj.type) {
      return true;
    }
  }
  return false;
}

exports.includeUserID = (obj, list) => {
  for (let e of list) {
    if (obj === e.id_user_send) {
      return true;
    }
  }
  return false;
}

exports.include = (obj, list) => {
  for (let element of list) {
    if (element.type === obj.type) {
      return element;
    }
  }
  return null;
}

exports.change = (obj, list) => {
  for (let element of list) {
    if (element.type === obj.type) {
      element.amount += 1;
      break;
    }
  }
}

exports.addTo = (obj, list) => {
  list.push({
    type      : obj.type,
    name      : obj.name,
    amount    : 1,
    is_gift   : obj.is_gift
  });
}

exports.addAll = (origin, target) => {
  for (let e of target) {
    origin.push(e);
  }
}

exports.filterMission = (strMission) => {
  try {
    
    let lsMission = JSON.parse(strMission);
    lsMission = lsMission.filter(e => e.type === 6 || e.type === 7);

    return JSON.stringify(lsMission);

  } catch (err) {
    console.log(err);
    return "[]";
  }
}