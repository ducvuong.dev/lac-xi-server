const redis           = require('../redis/redis');
const arrUtil         = require('../utils/array_util');
const inventoryFunc   = require('./inventory_func');

exports.merge = async (megaID, typeItem) => {

  if (typeItem === null || typeItem === undefined) {
    return null;
  }

  let strInventory = await redis.getInventory(megaID);
  if (strInventory === null || strInventory === undefined) {
    return null;
  }

  let lsInventory   = JSON.parse(strInventory);
  let itemInInven   = lsInventory.find(elemen => { return elemen.type === typeItem });

  const strAllItem  = await redis.getAllItem();
  const lsAllItem   = JSON.parse(strAllItem);
  const itemChk     = lsAllItem.find(e => { return e.type === typeItem });

  if (itemInInven === null || itemInInven === undefined ||
      itemChk     === null || itemChk     === undefined ||
      itemChk.upgrade === -1                        ||
      itemInInven.amount < itemChk.condi_merge) {
    return null;
  }

  itemInInven.amount -= itemChk.condi_merge;
  let itemUpdate      = lsAllItem.find(e => { return e.type === itemChk.upgrade });

  //update item in inventory
  if (arrUtil.inclcudes(itemUpdate, lsInventory) === true) {
    arrUtil.change(itemUpdate, lsInventory);
  }
  else {
    arrUtil.addTo(itemUpdate, lsInventory);
  }

  lsInventory = lsInventory.filter(e => e.amount > 0);
  return {
    inventories: lsInventory,
    type       : itemUpdate.type
  };

}

exports.mergeCardBillion = async (megaID) => {
  if (megaID === null || megaID === undefined) return null;

  let strInventory = await redis.getInventory(megaID);
  if (strInventory === null || strInventory === undefined) return null;

  let cardBillionExist = await redis.getCardBillion(megaID, 'card_billion');
  if (cardBillionExist !== null && cardBillionExist !== undefined) return null;

  let lsInventory = JSON.parse(strInventory);
  let ableMerge   = inventoryFunc.chkAbleMergeCardBillion(lsInventory);

  if (ableMerge === true) {
    let lsInventoryUpdate = updateLsInventory(lsInventory);
    await redis.updateUser(megaID, 'inventories', JSON.stringify(lsInventoryUpdate));
    await redis.updateUser(megaID, 'card_billion', '1');

    return lsInventoryUpdate;
  }
  return null;
}

function updateLsInventory(lsInventory) {
  for (let e of lsInventory) {
    if (e.type === 6 || e.type === 7 || e.type === 8 || e.type === 9) {
      e.amount -= 1;
    }
  }
  lsInventory = lsInventory.filter(e => e.amount > 0);
  return lsInventory;
}