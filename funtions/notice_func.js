const redis     = require('../redis/redis');
const arrUtil   = require('../utils/array_util');

exports.getAllNotice = async (megaID) => {
  let result = await redis.getNotice(megaID);
  if (result === null || result === undefined) {
    return null;
  }
  return JSON.parse(result);
}

exports.getGift = async (megaID, megaIDUserSend, type) => {
  if (type === null || type === undefined) return null;

  let strNotice = await redis.getNotice(megaID);
  if (strNotice === null || strNotice === undefined) return null;

  let lsNotice  = JSON.parse(strNotice);
  let gift      = lsNotice.find(e => { return e.type === type && e.id_user_send === megaIDUserSend });

  if (gift === null || gift === undefined) return null;
  
  let strInventory = await redis.getInventory(megaID);
  if (strInventory === null || strInventory === undefined) {

    let tmpInven = [];
    tmpInven.push({
      type    : gift.type,
      name    : gift.name,
      amount  : gift.amount,
      is_gift : true
    });

    lsNotice = lsNotice.filter(e => e.type !== gift.type);

    await redis.updateUser(megaID, 'notices', JSON.stringify(lsNotice));
    await redis.updateUser(megaID, 'inventories', JSON.stringify(tmpInven));
    return lsNotice;

  } //user chưa có túi
  else {

    let lsInventory = JSON.parse(strInventory);
    let itemIsExist = arrUtil.include(gift, lsInventory);
    if (itemIsExist === null) {
      lsInventory.push({
        type    : gift.type,
        name    : gift.name,
        amount  : gift.amount,
        is_gift : true
      });
    }
    else {
      itemIsExist.amount += gift.amount;
    }

    lsNotice = lsNotice.filter(e => e.type !== gift.type);

    await redis.updateUser(megaID, 'notices', JSON.stringify(lsNotice));
    await redis.updateUser(megaID, 'inventories', JSON.stringify(lsInventory));
    return lsNotice;

  } //user chưa có túi
}