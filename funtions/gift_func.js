const redis     = require('../redis/redis');
const firestore = require('../repository/firestore');
const arrUtil   = require('../utils/array_util');

exports.givingGift = async (megaID, type, amount, reciMegaID) => {

  if (megaID === reciMegaID                                   ||
      amount === null || amount === undefined || amount === 0 ||
      type   === null || type   === undefined) {
    return 'failed';
  }

  //TODO: change check user is exist
  let userSend  = await firestore.getUserBy(megaID);
  let userReci  = await firestore.getUserBy(reciMegaID);

  if (userSend === null || userReci === null) {
    return 'failed';
  }

  //check inventory user send gift
  let strInventoryUserSend = await redis.getInventory(megaID);
  if (strInventoryUserSend === null || strInventoryUserSend === undefined) {
    return 'failed';
  }

  let lsInventoryUserSend = JSON.parse(strInventoryUserSend);
  let strNoticeUserReci   = await redis.getNotice(reciMegaID);

  //check amount send with amount in inventory of user send gift
  let itemSend = lsInventoryUserSend.find(e => { return e.type === type && e.is_gift === true; });
  if (itemSend === null || itemSend === undefined || itemSend.amount < amount ) {
    return 'failed';
  }

  itemSend.amount -= amount;
  let lsInvenUpdateUserSend   = lsInventoryUserSend.filter(e => e.amount > 0);

  let strNoticeUpdateUserReci = updateNoticeUserReci(strNoticeUserReci, itemSend, amount, megaID);
  await redis.updateUser(megaID, 'inventories', JSON.stringify(lsInvenUpdateUserSend));
  await redis.updateUser(reciMegaID, 'notices', strNoticeUpdateUserReci);

  return lsInvenUpdateUserSend;
}

function updateNoticeUserReci(strNotice, gift, amount, idUserSend) {
  if (strNotice === null || strNotice === undefined) {
    let tmpNotice = [];
    tmpNotice.push({
      id_user_send  : idUserSend,
      type          : gift.type,
      name          : gift.name,
      amount        : amount   
    });
    return JSON.stringify(tmpNotice);
  }
  else {
    let lsNotice     = JSON.parse(strNotice);
    let itemInNotice = arrUtil.include(gift, lsNotice);
    if (itemInNotice !== null) {
      if (arrUtil.includeUserID(idUserSend, lsNotice) === true) {
        itemInNotice.amount += amount;
      }
      else {
        lsNotice.push({
          id_user_send  : idUserSend,
          type          : gift.type,
          name          : gift.name,
          amount        : amount
        });
      }
    }
    else {
      lsNotice.push({
        id_user_send  : idUserSend,
        type          : gift.type,
        name          : gift.name,
        amount        : amount
      });
    }
    return JSON.stringify(lsNotice);
  }
}