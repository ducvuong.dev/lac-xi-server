const redis     = require('../redis/redis');

exports.chkAbleMergeCardBillion = (lsInventory) => {
  let count = 0;
  for (let e of lsInventory) {
    if (e.type === 6 || e.type === 7 || e.type === 8 || e.type === 9) {
      count += 1;
    }
  }
  if (count === 4) {
    return true;
  }
  return false;
}