const redis         = require('../redis/redis');
const firestore     = require('../repository/firestore');
const { MISSIONS }  = require('../config');
const arrUtil       = require('../utils/array_util');

exports.updateMission = async (megaID, typeMission) => {

  let tMin1      = MISSIONS[1].condition[0];
  let tMax1      = MISSIONS[1].condition[1];
  let tMin2      = MISSIONS[1].condition[2];
  let tMax2      = MISSIONS[1].condition[3];

  if (typeMission === null || typeMission === undefined) return null;

  if (typeMission === 1 &&
     (timeInRange(tMin1, tMax1) === false && timeInRange(tMin2, tMax2) === false)) {
       return null;
     }

  let chkMissionExist = MISSIONS.find(e => { return e.type === typeMission });
  if (chkMissionExist === null || chkMissionExist === undefined) return null;

  let strMission    = await redis.getAllMissionUser(megaID);
  let strTurn       = await redis.getTurnUser(megaID);

  if (strMission === null || strMission === undefined) {
    let lsMissionUser = [];
    lsMissionUser.push({
      type          : chkMissionExist.type,
      description   : chkMissionExist.description,
      bonus         : chkMissionExist.bonus,
      status        : true
    });

    if (strTurn === null || strTurn === undefined) {
      let turn = chkMissionExist.bonus;
      redis.updateUser(megaID, 'turns', turn.toString());
      redis.updateUser(megaID, 'missions', JSON.stringify(lsMissionUser));

      return {
        turn  : turn,
        bonus : chkMissionExist.bonus
      };
    }
    else {
      let turn = parseInt(strTurn, 10);
      turn += chkMissionExist.bonus;
      redis.updateUser(megaID, 'turns', turn.toString());
      redis.updateUser(megaID, 'missions', JSON.stringify(lsMissionUser));

      return {
        turn  : turn,
        bonus : chkMissionExist.bonus
      };
    }
  } //user chưa có ls mission
  else {
    let lsMissionUser = JSON.parse(strMission);
    if (arrUtil.inclcudes(chkMissionExist, lsMissionUser) === true) {
      return null;
    }
    else {
      lsMissionUser.push({
        type          : chkMissionExist.type,
        description   : chkMissionExist.description,
        bonus         : chkMissionExist.bonus,
        status        : true
      });
      
      if (strTurn === null || strTurn === undefined) {
        let turn = chkMissionExist.bonus;
        redis.updateUser(megaID, 'turns', turn.toString());
        redis.updateUser(megaID, 'missions', JSON.stringify(lsMissionUser));
  
        return {
          turn  : turn,
          bonus : chkMissionExist.bonus
        };
      }
      else {
        let turn = parseInt(strTurn, 10);
        turn += chkMissionExist.bonus;

        redis.updateUser(megaID, 'turns', turn.toString());
        redis.updateUser(megaID, 'missions', JSON.stringify(lsMissionUser));
  
        return {
          turn  : turn,
          bonus : chkMissionExist.bonus
        };
      }
    }
  } //user đã có ls mission
}

exports.missionUnComplete = async (megaID) => {

  let strMission = await redis.getAllMissionUser(megaID);
  let tMin1      = MISSIONS[1].condition[0];
  let tMax1      = MISSIONS[1].condition[1];
  let tMin2      = MISSIONS[1].condition[2];
  let tMax2      = MISSIONS[1].condition[3];

  if (strMission === null || strMission === undefined) {
    let tmpMission = addAllMission();
    if (timeInRange(tMin1, tMax1) === false && timeInRange(tMin2, tMax2) === false) {
      let item = tmpMission.find(e => { return e.type === 1 });
      item.status = true;
    }
    return tmpMission;
  }

  let lsMission           = JSON.parse(strMission);
  let lsMissionUnComplete = filterLsMissionUnComplete(lsMission);
  
  if (arrUtil.inclcudes(MISSIONS[1], lsMissionUnComplete) === true) {

    if (timeInRange(tMin1, tMax1) === false && timeInRange(tMin2, tMax2) === false) {
      let tmpMission = lsMissionUnComplete.find(e => { return e.type === 1 });
      tmpMission.status = true;
    }

  }
  else {
    lsMissionUnComplete.push({
      type          : MISSIONS[1].type,
      description   : MISSIONS[1].description,
      bonus         : MISSIONS[1].bonus,
      status        : true
    });
  }

  if (arrUtil.inclcudes(MISSIONS[0], lsMissionUnComplete) === false) {
    lsMissionUnComplete.push({
      type          : MISSIONS[0].type,
      description   : MISSIONS[0].description,
      bonus         : MISSIONS[0].bonus,
      status        : true
    });
  }

  return lsMissionUnComplete;
}

function filterLsMissionUnComplete(lsMissionComplete) {
  let lsMissionUnComplete = [];
  for (let e of MISSIONS) {
    if (arrUtil.inclcudes(e, lsMissionComplete) === false) {
      if (e.type === 7) {
        let questionFilter = filterQuestion(e.question);
        lsMissionUnComplete.push({
          type          : e.type,
          description   : e.description,
          bonus         : e.bonus,
          status        : false,
          question      : questionFilter
        });
      }
      else {
        lsMissionUnComplete.push({
          type          : e.type,
          description   : e.description,
          bonus         : e.bonus,
          status        : false,
          question      : []
        });
      }
    }
  }
  return lsMissionUnComplete;
}

function addAllMission() {
  let lsMission = [];
  for (let item of MISSIONS) {
    if (item.type === 7) {
      let questionFilter = filterQuestion(item.question);
      lsMission.push({
        type          : item.type,
        description   : item.description,
        bonus         : item.bonus,
        status        : false,
        question      : questionFilter
      });
    }
    else {
      lsMission.push({
        type          : item.type,
        description   : item.description,
        bonus         : item.bonus,
        status        : false,
        question      : []
      });
    }
  }
  return lsMission;
}

function timeInRange(hMin, hMax) {
  let time  = new Date();
  let tMin  = new Date();
  let tMax  = new Date();

  tMin.setHours(hMin, 0, 0, 0, 0);
  tMax.setHours(hMax, 0, 0, 0, 0);

  if (time.getTime() >= tMin.getTime() && time.getTime() <= tMax.getTime()) {
    return true;
  }
  return false;
}

function filterQuestion(questions) {
  let tmp = questions.filter(e => e.hide === false);
  return tmp;
}