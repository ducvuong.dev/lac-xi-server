const redis      = require('../redis/redis');
const arryUtil   = require('../utils/array_util');

exports.calItemWhenTurn = (lsItem) => {
  let   rnd     = Math.round(Math.random() * 1000) + 1;
  let   percent = 0;

  let tmpItem = lsItem.find(e => { return e.maximum === -1; });
  for (let item of lsItem) {
    percent += item.percent;
    if (rnd < percent) {
      tmpItem = item;
      break;
    }
  }

  if (tmpItem.amount === tmpItem.maximum) {
    tmpItem = lsItem.find(e => { return e.maximum === -1; });
  }

  tmpItem.amount += 1;
  redis.updateLsItem(lsItem);
  
  return tmpItem;
}

exports.initInventory = (item) => {
  let inventories = [];
  inventories.push({
    type    : item.type,
    name    : item.name,
    amount  : 1,
    is_gift : item.is_gift
  });
  return JSON.stringify(inventories);
}

exports.initHistory = (item) => {
  let date        = new Date();
  let histories = [];
  histories.push({
    name  : item.name,
    date  : date.getTime()
  });
  return JSON.stringify(histories);
}

exports.updateHistory = (histories, item) => {
  let date      = new Date();
  let lsHistory = JSON.parse(histories);
  lsHistory.push({
    name: item.name,
    date: date.getTime()
  });
  
  return JSON.stringify(lsHistory);
}

exports.updateInventory = (inventory, item) => {
  let lsInventory = JSON.parse(inventory);
  let itemChange  = arryUtil.include(item, lsInventory);

  if (itemChange === null || itemChange === undefined) {
    lsInventory.push({
      name  : item.name,
      type  : item.type,
      amount: 1,
      is_gift : item.is_gift
    });
  }
  else {
    itemChange.amount += 1;
  }
  return JSON.stringify(lsInventory);
}