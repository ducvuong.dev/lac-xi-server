const express         = require('express');
const router          = express.Router();
const giftFunc        = require('../funtions/gift_func');

router.post('/giving-gift', async (req, res) => {

  let megaID      = req.body.megaID;
  let type        = req.body.type;
  let amount      = req.body.amount;
  let reciMegaID  = req.body.reciMegaID;

  let result      = await giftFunc.givingGift(megaID.toString().trim(),
                                              parseInt(type.toString().trim(), 10),
                                              parseInt(amount.toString().trim(), 10),
                                              reciMegaID.toString().trim()
                                              );
  res.json({
    result: result
  });

});

module.exports        = router;