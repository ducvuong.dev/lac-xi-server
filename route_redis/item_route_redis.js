const express       = require('express');
const router        = express.Router();
const itemFunc      = require('../funtions/item_func');
const client        = require('../redis/redis');
const inventoryFunc = require('../funtions/inventory_func');
const config        = require('../config');

router.post('/merge-item', async (req, res) => {

  try {
    
    const typeItem  = req.body.type;
    const megaID    = req.body.megaID;
    const result    = await itemFunc.merge(megaID, parseInt(typeItem, 10));
  
    if (result === null || result === undefined) {
      res.json({
        result: []
      });
    }
    else {
      client.updateUser(megaID, 'inventories', JSON.stringify(result.inventories));

      let ableMergeCardBillion = inventoryFunc.chkAbleMergeCardBillion(result.inventories);
      let cardBillion          = await client.getCardBillion(megaID.toString().trim(), 'card_billion');

      if (ableMergeCardBillion === true) {
        if (cardBillion === null || cardBillion === undefined) {
          res.json({
            result                    : result.inventories,
            type                      : result.type,
            button_merge_card_billion : true,
            card_billion              : false
          });
        }
        else {
          res.json({
            result                    : result.inventories,
            type                      : result.type,
            button_merge_card_billion : false,
            card_billion              : true
          });
        }
      }
      else {
        if (cardBillion === null || cardBillion === undefined) {
          res.json({
            result                    : result.inventories,
            type                      : result.type,
            button_merge_card_billion : false,
            card_billion              : false
          });
        }
        else {
          res.json({
            result                    : result.inventories,
            type                      : result.type,
            button_merge_card_billion : false,
            card_billion              : true
          });
        }
      }
    }
  
    
  }
  catch(err) {
    console.log(err);
  }
  
});

router.post('/merge-card-billion', async (req, res) => {
  let megaID                = req.body.megaID;
  let lsInventoryAfterMerge = await itemFunc.mergeCardBillion(megaID.toString().trim());

  if (lsInventoryAfterMerge === null) {
    res.json({
      result  : []
    });
  }
  else {
    res.json({
      result                    : lsInventoryAfterMerge,
      button_merge_card_billion : false,
      card_billion              : true
    });
  }
});

router.post('/change-item', (req, res) => {
  // let type  = req.body.type
  // let item  = config.ARR_ITEM.find(e => { return e.type === type });

  // item.maximum = 1100;
  let a = '';

  console.log(req);

  res.send(a);

});

router.get('/get-all-item', (req, res) => {
  res.json({
    result: config.ARR_ITEM
  })
});

module.exports = router;