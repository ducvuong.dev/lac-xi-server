const client        = require('../redis/redis');
const express       = require('express');
const router        = express.Router();

const db            = require('../repository/firestore');

const wheelFunc     = require('../funtions/wheel_func');

router.post('/get-item', async (req, res) => {
  
  let megaID    = req.body.megaID;

  let strTurn   = await client.getTurnUser(megaID.toString().trim());
  if (strTurn === null || strTurn === undefined) {
    //TODO: check megaID in host mega => if true => initUser else => returns
    res.json({
      result: 'failed'
    });
  }
  else {

    let turn = parseInt(strTurn, 10);
    if (turn === 0) {
      res.json({
        result: 'failed',
        turn  : turn
      });
    }
    else {

      let lsItem      = await client.getAllItem();
      let item        = wheelFunc.calItemWhenTurn(JSON.parse(lsItem));

      if (lsItem === null || lsItem === undefined ||
          item   === null || item   === undefined) {
            res.json({
              result: 'failed',
              item  : 'can not get item'
            });
      }
      else {

        let strInventory  = '[]';
        let strHistory    = '[]';

        //check inventory
        let inventory = await client.getInventory(megaID);
        if (inventory === null || inventory === undefined) {
          strInventory = wheelFunc.initInventory(item);
        }
        else {
          strInventory = wheelFunc.updateInventory(inventory, item);
        }

        // check history
        let history = await client.getHistoryUser(megaID);
        if (history === null || history === undefined) {
          if (item.save_in_gift === true) {
            strHistory = wheelFunc.initHistory(item);
          }
        }
        else {
          if (item.save_in_gift === true) {
            strHistory = wheelFunc.updateHistory(history, item);
          }
          else {
            strHistory = history;
          }
        }

        turn -= 1;
        client.updateMultiFieldUser(megaID, 'inventories', strInventory,
                                            'histories', strHistory,
                                            'turns', turn.toString());

        res.json({
          type: item.type,
          name: item.name,
          turn: turn
        });

      }

    }

  }

});

router.get('/get-all-item', async (req, res) => {
  let tmpLsItem = await client.getAllItem();
  let lsItem    = JSON.parse(tmpLsItem);
  res.json(lsItem);
});

router.post('/get-turn', async (req, res) => {
  let   megaID  = req.body.megaID;
  const strTurn = await client.getTurnUser(megaID.toString().trim());
  if (strTurn === null || strTurn === undefined) {
    res.json({
      result  : 0,
      strTurn : 'null or undefined strTurn'
    });
  }
  else {
    res.json({
      result: parseInt(strTurn, 10)
    });
  }
});

router.post('/get-history', async (req, res) => {
  const megaID      = req.body.megaID;
  let strHistory  = await client.getHistoryUser(megaID);
  let lsHistory   = JSON.parse(strHistory);

  if (strHistory === null || strHistory === undefined) {
    res.json({
      result: 'failed'
    });
  }
  else {
    res.json({
      result: lsHistory
    });
  }
});

module.exports = router;