const express         = require('express');
const router          = express.Router();
const client          = require('../redis/redis');
const inventoryFunc   = require('../funtions/inventory_func');

router.post('/get-inventory', async (req, res) => {
  let   megaID = req.body.megaID;
  const result = await client.getInventory(megaID.toString().trim());

  if (result === null || result === undefined) {
    res.json({
      result: 'failed',
      inven : 'inventory is null'
    });
  }
  else {

    let lsInventory          = JSON.parse(result);
    let ableMergeCardBillion = inventoryFunc.chkAbleMergeCardBillion(lsInventory);
    let cardBillion          = await client.getCardBillion(megaID.toString().trim(), 'card_billion');

    if (ableMergeCardBillion === true) {
      if (cardBillion === null || cardBillion === undefined) {
        res.json({
          result                    : lsInventory,
          button_merge_card_billion : true,
          card_billion              : false
        });
      }
      else {
        res.json({
          result                    : lsInventory,
          button_merge_card_billion : false,
          card_billion              : true
        });
      }
    }
    else {
      if (cardBillion === null || cardBillion === undefined) {
        res.json({
          result                    : lsInventory,
          button_merge_card_billion : false,
          card_billion              : false
        });
      }
      else {
        res.json({
          result                    : lsInventory,
          button_merge_card_billion : false,
          card_billion              : true
        });
      }
    }
  }
});

router.post('/set-card-billion', async (req, res) => {
  let inventory = [];
  let i1 = {
    type      : 6,
    name      : 'Thẻ Long',
    amount    : 1,
    is_gift   : true
  }
  let i2 = {
    type      : 7,
    name      : 'Thẻ Lân',
    amount    : 1,
    is_gift   : true
  }
  let i3 = {
    type      : 8,
    name      : 'Thẻ Quy',
    amount    : 1,
    is_gift   : true
  }
  let i4 = {
    type      : 9,
    name      : 'Thẻ Phụng',
    amount    : 1,
    is_gift   : true
  }

  inventory.push(i1);
  inventory.push(i2);
  inventory.push(i3);
  inventory.push(i4);

  await client.updateUser(req.body.megaID, 'inventories', JSON.stringify(inventory));
  res.json({
    result: 'ok'
  });
});

module.exports = router;