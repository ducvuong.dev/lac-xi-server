const express       = require('express');
const router        = express.Router();
const client        = require('../redis/redis');
const missionFunc   = require('../funtions/mission_func');
const convertTime   = require('../utils/convert_milliseconds');
const { MISSIONS }  = require('../config');
const firestore     = require('../repository/firestore');

router.post('/join-mission', async (req, res) => {
  const megaID            = req.body.megaID;
  const typeMission       = req.body.type;

  let userExist           = await firestore.getUserBy(megaID.toString().trim());
  let resultUpdateMission = await missionFunc.updateMission(megaID.toString().trim(),
                                                            parseInt(typeMission, 10));
                                                            
  if (resultUpdateMission === null || userExist === null) {
    res.json({
      result: 'failed'
    });
  }
  else {
    let lsMissionUnComplete = await missionFunc.missionUnComplete(megaID.toString().trim());
    res.json({
      turn      : resultUpdateMission.turn,
      bonus     : resultUpdateMission.bonus,
      result    : lsMissionUnComplete
    });
  }

});

router.post('/get-all-mission', async (req, res) => {

  let megaID    = req.body.megaID;

  //TODO: change check user exist
  let userExist = await firestore.getUserBy(megaID.toString().trim());

  if (megaID === null || megaID === undefined || userExist === null) {
    res.json({
      result: []
    });
  }
  else {
    let lsMission = await missionFunc.missionUnComplete(megaID.toString().trim());
    res.json({
      result: lsMission
    });
  }

});

router.post('/get-mission-complete', async (req, res) => {
  let strMission = await client.getAllMissionUser(req.body.megaID);
  res.json({
    result: JSON.parse(strMission)
  })
});

router.get('/reset-mission', (req, res) => {
  client.resetMisison();
  res.json({
    result: 'ok'
  });
});

module.exports = router;