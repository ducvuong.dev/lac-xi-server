const express     = require('express');
const router      = express.Router();
const noticeFunc  = require('../funtions/notice_func');
const client      = require('../redis/redis');

router.post('/get-all-notice', async (req, res) => {
  let megaID      = req.body.megaID;
  let result      = await noticeFunc.getAllNotice(megaID.toString().trim());

  if (result === null) {
    res.json({
      result: 'failed'
    });
  }
  else {
    res.json({
      result: result
    })
  }
});

router.post('/get-gift', async (req, res) => {
  let megaID           = req.body.megaID;
  let megaIDUserSend   = req.body.megaIDUserSend;
  let type             = req.body.type;

  let lsNoticeUpdate   = await noticeFunc.getGift(megaID.toString().trim(),
                                                  megaIDUserSend.toString().trim(),
                                                  parseInt(type, 10));

  if (lsNoticeUpdate === null) {
    res.json({
      result: 'failed'
    });
  }
  else {
    res.json({
      result: lsNoticeUpdate
    });
  }

});

module.exports    = router;